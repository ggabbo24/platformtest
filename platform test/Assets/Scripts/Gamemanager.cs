﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gamemanager : MonoBehaviour
{
    // Start is called before the first frame update
    public Text displayedTitle;
    public Text displayedSubtitle;

    public GameObject player;
    private PlayerMov playerStats;


    
    void Start()
    {
        playerStats = player.GetComponent<PlayerMov>();
        
    }
    private void GameEnding()
    {
        if (playerStats.finishedLevel)
        {
            displayedTitle.text = "Hai Vinto!";
            displayedSubtitle.text = "Congratulazioni!";
            Invoke("NextLevel",2f);
        }
        else
        {
            displayedTitle.text = "Hai perso!";
            displayedSubtitle.text = "Riprovaci!";
            Invoke("ToMenu", 2f);
        }
        

    }

    private void NextLevel()
    {
        SceneManager.LoadScene(2);
    }

    private void ToMenu()
    {
        SceneManager.LoadScene(0);
        Destroy(GameObject.Find("BgMusic"));
    }


    
}

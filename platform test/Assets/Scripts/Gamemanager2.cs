﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Gamemanager2 : MonoBehaviour
{
    // Start is called before the first frame update
    public Text displayedTitle;
    public Text displayedSubtitle;

    public GameObject player;
    public GameObject startpoint;
    public Transform spawnPosition;
    public CameraScript mCamera;
    private PlayerMov playerStats;
   

    void Start()
    {


        StartCoroutine(GameLoop());


    }
    private IEnumerator GameLoop()
    {
        // yield return StartCoroutine(GameStarting());

        yield return StartCoroutine(GameStarting());
        yield return StartCoroutine(GamePlaying());
        yield return StartCoroutine(GameEnding());



    }


    private IEnumerator GameStarting()
    {
        yield return new WaitForSeconds(3);

    }

    private IEnumerator GamePlaying()
    {
        player = Instantiate(player, spawnPosition) as GameObject;
        playerStats = player.GetComponent<PlayerMov>();
        mCamera.player = player;
        displayedTitle.text = "";
        displayedSubtitle.text = "";
        while (player.activeSelf)
        {
            yield return null;
        }
    }

    private IEnumerator GameEnding()
    {
        if (playerStats.finishedLevel)
        {
            displayedTitle.text = "Hai Vinto!";
            displayedSubtitle.text = "Congratulazioni!";
            yield return new WaitForSeconds(5);
            SceneManager.LoadScene(0);
        }
        else
        {
            displayedTitle.text = "Hai perso!";
            displayedSubtitle.text = "Riprovaci!";
        }

        yield return new WaitForSeconds(5);
        SceneManager.LoadScene(2);

    }



}

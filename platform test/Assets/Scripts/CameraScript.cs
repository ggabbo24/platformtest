﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{

    public GameObject player = null;

    private Vector2 offset;

    void Start()
    {
        offset = new Vector2 (transform.position.x - player.transform.position.x,transform.position.y - player.transform.position.y);
    }

    void LateUpdate()
    {
        transform.position = new Vector3 (player.transform.position.x + offset.x, player.transform.position.y + offset.y, -10);
        
    }
}
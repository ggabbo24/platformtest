﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableScript : MonoBehaviour
{
    // Start is called before the first frame update

    public int pointValue = 1;
    // Update is called once per frame
    private void Start()
    {
        StartCoroutine(Behaviour());
    }

    private IEnumerator Behaviour()
    {
        for(int i = 0; i <360; i+=30 )
        { 
        transform.DORotate(new Vector3(i, i, i), 0.3f);
        yield return new WaitForSeconds(0.3f);
        }
        yield return StartCoroutine(Behaviour());
    }
    //Quando il giocatore tocca il collezionabile, il valore del collezionabile viene aggiunto al punteggio del giocatore
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            int pPoints = other.gameObject.GetComponent<PlayerMov>().playerPoints;
            pPoints = pPoints  + pointValue;
            other.gameObject.GetComponent<PlayerMov>().currentPoints.text = "Punti: " + pPoints;
            other.gameObject.GetComponent<PlayerMov>().playerPoints = pPoints;
            this.gameObject.SetActive(false);
        }
    }
}

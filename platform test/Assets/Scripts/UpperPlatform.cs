﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpperPlatform : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            coll.collider.transform.SetParent(transform);
        }
    }
    private void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            coll.collider.transform.SetParent(null);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMov : MonoBehaviour
{

    private Rigidbody2D player ;
    public float jumpInput;
    public float jumpPower;
    public float movSpeed;

    public Text currentPoints;
    [HideInInspector]public int playerPoints = 0;
    [HideInInspector]public Boolean finishedLevel = false;

    public AudioSource audioSource;
    public AudioClip jumpStartSound;
    public AudioClip jumpEndSound;

    public LayerMask whatIsGround;
    public Transform groundCheck;
    public float checkRadius;

    public FixedJoystick joystick;
  
    public Boolean canJump = false;
    

    

    // Start is called before the first frame update

    void Start()
    {
        
        currentPoints.text = "Punti: " + playerPoints;
    }

    private void Awake()
    {
        player = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
        jumpInput = Mathf.Ceil(Input.GetAxis("Vertical"));

        canJump = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround);


    }

    private void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.RightArrow) || joystick.Horizontal > 0.3)
        {
                Move("right");
        }


        //Right movement thorough Left arrow key
        if (Input.GetKey(KeyCode.LeftArrow) || joystick.Horizontal < -0.3f)
        {
                Move("left");
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
 
                Jump();
        }
    }

    private void Move(string key)
    {
        Vector2 playerposition = new Vector2(transform.position.x * Time.deltaTime, transform.position.y);
        if (key == "left")
        {
            playerposition.x = transform.position.x - movSpeed / 100;
        }
        else if (key == "right")
        {
            playerposition.x = transform.position.x + movSpeed / 100;
        }
        transform.position = playerposition;
    }
    
    public void Jump()
    {
        if (canJump)
        {
            player.AddForce(new Vector2(0f, jumpPower), ForceMode2D.Impulse);
            audioSource.clip = jumpStartSound;
            audioSource.Play();
        }
    }
  
    private void OnTriggerEnter2D(Collider2D other)
    {
        
        if (other.gameObject.tag == "WaterHazard")
        {
            this.gameObject.SetActive(false);
            GameObject.Find("GameManager").SendMessage("GameEnding");
            
        }
        if (other.gameObject.tag == "EndLevel")
        {
            this.gameObject.SetActive(false);
            finishedLevel = true;
            GameObject.Find("GameManager").SendMessage("GameEnding");
        }
    }
}

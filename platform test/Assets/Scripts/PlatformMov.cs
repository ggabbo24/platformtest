﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMov : MonoBehaviour
{
    // Start is called before the first frame update

    // Update is called once per frame


  
    private void Start()
    {
        StartCoroutine(Behaviour());
    }

    private IEnumerator Behaviour()
    {
       
            transform.DOMove(new Vector3(transform.position.x+15, transform.position.y, transform.position.z), 5f);
            yield return new WaitForSeconds(6);
            transform.DOMove(new Vector3(transform.position.x - 15, transform.position.y, transform.position.z), 5f);
            yield return new WaitForSeconds(6);
            yield return StartCoroutine(Behaviour());
    }
    private void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            coll.collider.transform.SetParent(transform);
        }
    }
    private void OnCollisionExit2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            coll.collider.transform.SetParent(null);
        }
    }
}

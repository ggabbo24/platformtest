﻿
using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class ARLesson : MonoBehaviour
{
    [SerializeField] TMPro.TMP_Text StateText;
    [SerializeField] TMPro.TMP_Text DynamicText;
    [SerializeField] ARPlaneManager planeManager;
    [SerializeField] ARPointCloudManager cloudManager;
    [SerializeField] ARRaycastManager raycastManager;
    [SerializeField] Transform spawnRobot;
    [SerializeField] ARCameraManager cameraManager;
    [SerializeField] Light dirLight;
    [SerializeField] Image imageLight;
    [SerializeField] GameObject ballPrefab;

    private int currentPlaneNum = 0;
    private int currentPointNum = 0;
    private Touch touch;
    private Transform robot = null;
    public Boolean rotate;
    public Text buttonText;

    private float avgBright;
    private float avgColor;
    private Color colorCorr;
    

    private void Start()
    {
        
        ARSession.stateChanged += UpdateStateText;
        planeManager.planesChanged += UpdatePlaneNumber;
        cloudManager.pointCloudsChanged += UpdatePointNumber;
        cameraManager.frameReceived += LightEstimation;
       
        UpdateText();
    }

    private void Update()
    {
        
         if (Input.touchCount == 1 && !IsPointerOverUIObject())
            {
                touch = Input.GetTouch(0);
                var hits = new List<ARRaycastHit>();
                raycastManager.Raycast(touch.position, hits, TrackableType.Planes);
            if (hits.Count > 0)
                {
                if (robot == null)
                    robot = GameObject.Instantiate(spawnRobot, hits[0].pose.position, hits[0].pose.rotation);
                else
                    robot.position = hits[0].pose.position;
                }
            }

        if (Input.touchCount >= 2)
        {
            var pos1 = Input.GetTouch(0).position;
            var pos2 = Input.GetTouch(1).position;
            var pos1b = Input.GetTouch(0).position - Input.GetTouch(0).deltaPosition;
            var pos2b = Input.GetTouch(1).position - Input.GetTouch(1).deltaPosition;

            if (!rotate)
            {
                var scale = Vector3.Distance(pos1, pos2) /
                           Vector3.Distance(pos1b, pos2b);

                if (scale == 0 || scale > 10)
                    return;

                robot.transform.localScale = new Vector3(robot.transform.localScale.x * scale, robot.transform.localScale.y * scale, robot.transform.localScale.z * scale);
            }
            else
            {
                var angle = Vector2.SignedAngle(pos2- pos1, pos2b - pos1b);

                robot.transform.RotateAround(robot.position,Vector3.up,angle );
            }
            


        }
        dirLight.intensity = avgBright + 0.5f;
        dirLight.colorTemperature = avgColor;
        dirLight.color = colorCorr;

        imageLight.color = colorCorr;
        
    }

    public void ShootBall()
    {
        GameObject newBall = Instantiate<GameObject>(ballPrefab);
        newBall.transform.position = Camera.main.transform.position;
        Rigidbody rb = newBall.GetComponent<Rigidbody>();
        rb.AddForce(5000 * Camera.main.transform.forward);
    }
    private void UpdateStateText(ARSessionStateChangedEventArgs stato)
    {
        StateText.text = stato.state.ToString();
    }

    private void UpdatePlaneNumber(ARPlanesChangedEventArgs planeNum)
    {
        currentPlaneNum += planeNum.added.Count - planeNum.removed.Count;
        UpdateText();

    }

    private void UpdatePointNumber(ARPointCloudChangedEventArgs pointNum)
    {
        currentPointNum += pointNum.added.Count - pointNum.removed.Count;
        UpdateText();
    }

    private void LightEstimation(ARCameraFrameEventArgs cameraFrame)
    {
       if (cameraFrame.lightEstimation.averageBrightness != null)
            avgBright = cameraFrame.lightEstimation.averageBrightness.Value;
       if (cameraFrame.lightEstimation.averageColorTemperature != null)
            avgColor = cameraFrame.lightEstimation.averageColorTemperature.Value;
       if (cameraFrame.lightEstimation.colorCorrection != null)
            colorCorr = cameraFrame.lightEstimation.colorCorrection.Value;
    }
    private void UpdateText()
    {
        DynamicText.text = "PL: " + currentPlaneNum + " PC: " + currentPointNum;
    }
    public void rotateButton()
    {
        if (rotate == true)
        {
            rotate = false;
            buttonText.text = "Rotate";
        }
        else 
        {
            rotate = true;
            buttonText.text = "Scale";
        }
    }
    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.GetTouch(0).position.x, Input.GetTouch(0).position.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
}
